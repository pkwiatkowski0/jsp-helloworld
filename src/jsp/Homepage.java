package jsp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by patryk on 02.03.2017.
 */
@WebServlet({"/Homepage", "/index", "/about", "/contact", "/gallery"})
public class Homepage extends HttpServlet {
    public Homepage() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

//        String action = "action=";
//        String temp;
        String link = request.getParameter("action");
        //switch()
        switch (link) {
            case "gallery":
                response.sendRedirect("gallery.jsp");
                break;
            case "index":
                response.sendRedirect("index.jsp");
                break;
            case "about":
                response.sendRedirect("about.jsp");
                break;
            case "contact":
                response.sendRedirect("contact.jsp");
                break;

        }
//        if(getServletContext().equals("index")) {
//            response.getHeader("");
    }

}
