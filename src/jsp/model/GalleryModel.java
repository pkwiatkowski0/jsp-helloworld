package jsp.model;

import javax.servlet.annotation.WebServlet;
import java.util.LinkedList;

/**
 * Created by patryk on 04.03.2017.
 */
public class GalleryModel {

    private String name;
    private LinkedList<Image> images;

    public GalleryModel() {
    }

    public GalleryModel(String name) { //dodaje tylko pole name bo dodawanie obrazkow zrobie pozniej
        this.name = name;
        this.images = new LinkedList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LinkedList<Image> getImages() {
        return images;
    }

    public void setImages(LinkedList<Image> images) {
        this.images = images;
    }

    public void addImage(String url, String capt) {
        Image newImage = new Image(url, capt);
        images.add(newImage);
    }
}

