package jsp.model;

/**
 * Created by patryk on 04.03.2017.
 */
public class Image {

    private String url;
    private String caption;

    public Image() {
    }

    public Image(String url, String caption) {
        this.url = url;
        this.caption = caption;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }
}
