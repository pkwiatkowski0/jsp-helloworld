package jsp.model;

import sun.awt.image.ImageWatched;

import java.util.LinkedList;

/**
 * Created by patry on 04.03.2017.
 */
public class PersonModel {

    private String titleName;
    private String name;
    private String lastName;
    private int bornYear;
    private int phoneNumber;



    private Sex sex;
    private LinkedList<PersonModel> people;

    public PersonModel() {
    }

    public PersonModel(String titleName) {
        this.titleName = titleName;
        this.people = new LinkedList<>();
    }

    public String getTitleName() {
        return titleName;
    }

    public void setTitleName(String titleName) {
        this.titleName = titleName;
    }


    public PersonModel(String name, String lastName, int bornYear, int phoneNumber, Sex sex) {
        this.name = name;
        this.lastName = lastName;
        this.bornYear = bornYear;
        this.phoneNumber = phoneNumber;
        this.sex = sex;
    }

    public LinkedList<PersonModel> getPeople() {
        return people;
    }

    public void addPerson(String name, String lastName, int bornYear, int phoneNumber, Sex sex) {
        PersonModel newPerson = new PersonModel(name, lastName, bornYear, phoneNumber, sex);
        people.add(newPerson);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getBornYear() {
        return bornYear;
    }

    public void setBornYear(int bornYear) {
        this.bornYear = bornYear;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Sex getSex() {
        return sex;
    }
}
