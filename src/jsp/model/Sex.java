package jsp.model;

/**
 * Created by patry on 04.03.2017.
 */
public enum Sex {
    MALE("M"), FEMALE("F");

    private String text;

    Sex(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public static Sex fromString(String text) {
        for (Sex s : Sex.values()) {
            if (s.text.equalsIgnoreCase(text)) {
                return s;
            }
        }
        return null;
    }
}
