package jsp.servlet;

import jsp.model.PersonModel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by patry on 04.03.2017.
 */
@WebServlet({"AboutServlet"})
public class AboutServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PersonModel personModel = new PersonModel();
        request.setAttribute("about_model", personModel); //"person_model"- nazwa dla obiektu galleryModel

        request.getRequestDispatcher("about.jsp").forward(request, response);
    }
}
