package jsp.servlet;


import jsp.model.GalleryModel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Created by patryk on 04.03.2017.
 */
@WebServlet({"/GalleryServlet"})
public class GalleryServlet extends HttpServlet {

    public GalleryServlet() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        GalleryModel galleryModel = new GalleryModel("First gallery");
        galleryModel.addImage("https://i.ytimg.com/vi/n174VWdSlCE/maxresdefault.jpg", "Wonsz!");
        galleryModel.addImage("https://static.pexels.com/photos/34482/giraffe-animals-zoo-funny.jpg", "Żyrafy");
        galleryModel.addImage("http://weknowyourdreams.com/images/animals/animals-01.jpg", "Tiger");
        galleryModel.addImage("http://x3.cdn03.imgwykop.pl/c3201142/comment_AdXDn1ZeV8XK8eq1EN6UD7oZdjdqNYop.jpg", "Polak");
        galleryModel.addImage("http://ichef.bbci.co.uk/naturelibrary/images/ic/credit/640x395/a/an/animal/animal_1.jpg", "Słoń");
        request.setAttribute("gallery_model", galleryModel); //"gallery_model"- nazwa dla obiektu galleryModel
        //przekirowanie do servera. gallery.jsp prześle w response strone do klienta
        request.getRequestDispatcher("gallery.jsp").forward(request, response); //przekierowanie do innej strony

    }

}
