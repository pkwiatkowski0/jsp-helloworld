<%@ page import="jsp.model.PersonModel" %>
<%@ page import="jsp.model.Sex" %><%--
  Created by IntelliJ IDEA.
  User: patry
  Date: 02.03.2017
  Time: 19:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>About</title>
</head>
<body>
<h1>About</h1>
<%
    PersonModel personModel = (PersonModel) request.getAttribute("about_model");

%>

<h3>Name: <%= personModel.getTitleName() %>
</h3>
<table style="width: 50%; border: thick solid black" ; cellpadding="5" ; cellspacing="5">

    <tr style="text-align:left; background-color: coral">
        <th><h2>Name</h2></th>
        <th><h2>Surname</h2></th>
        <th><h2>BornYear</h2></th>
        <th><h2>Phone</h2></th>
        <th><h2>Gender</h2></th>
    </tr>
<%  %>

        <% for (PersonModel pModel : personModel.getPeople()) { %>
    <tr style="text-align:center">
        <td style="width: auto; border: 1px solid #000">
            <h3><%= pModel.getName() %>
            </h3>
        </td>
        <td style="width: auto; border: 1px solid #000">
            <h3><%= pModel.getLastName() %>
            </h3>
        </td>

        <td style="width: auto; border: 1px solid #000">
            <h3><%= pModel.getBornYear() %>
            </h3>
        </td>
        <td style="width: auto; border: 1px solid #000">

            <h3><%= pModel.getPhoneNumber() %>
            </h3>
        </td>
        <td style="width: auto; border: 1px solid #000">
            <h3><%= pModel.getSex() %>
            </h3>
        </td>
    </tr>
        <% } %>
</body>
</html>
