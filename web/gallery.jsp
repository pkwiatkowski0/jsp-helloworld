<%@ page import="jsp.servlet.GalleryServlet" %>
<%@ page import="jsp.model.GalleryModel" %>
<%@ page import="jsp.model.Image" %><%--
  Created by IntelliJ IDEA.
  User: patry
  Date: 02.03.2017
  Time: 19:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Gallery</title>
</head>
<body>
<h1>Gallery</h1>
<%
    GalleryModel galleryModel = (GalleryModel) request.getAttribute("gallery_model");
%>
<h3>Name: <%= galleryModel.getName() %>
</h3>

<br>

<table style="width:30%; border:solid">
    <% for (Image image : galleryModel.getImages()) { %>
    <tr>
        <td style="border: dashed green;">
            <img src="<%= image.getUrl() %>" width="100%" title="<%= image.getCaption()%>"/>
        </td>
    </tr>
    <% } %>
</table>
</body>
</html>
